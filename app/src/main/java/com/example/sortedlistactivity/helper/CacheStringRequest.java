package com.example.sortedlistactivity.helper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;


public class CacheStringRequest extends Request<NetworkResponse> {
    private final Response.Listener<NetworkResponse> mListener;
    private final Response.ErrorListener mErrorListener;
    private Context mContext;
    String postParams;
    private Cache.Entry cacheEntry;
    private String mContentType;
    private String mRequestBody = "";

    public CacheStringRequest(Activity activity, int method, String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.mListener = listener;
        this.mErrorListener = errorListener;
        this.mContext = activity;
        mContentType = "application/json";

    }

    public CacheStringRequest(int method, String url, String postParams, Response.Listener<NetworkResponse> networkResponseListener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.postParams = postParams;
        this.mListener = networkResponseListener;
        this.mErrorListener = errorListener;
        setShouldCache(false);
    }

    @Override
    protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
        cacheEntry = HttpHeaderParser.parseCacheHeaders(response);

        // I do this to ignore "no-cache" headers
        // and use built-in cache from Volley.
        if (cacheEntry == null) {
            cacheEntry = new Cache.Entry();
        }
        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
        long now = System.currentTimeMillis();
        final long softExpire = now + cacheHitButRefreshed;
        final long ttl = now + cacheExpired;
        cacheEntry.data = response.data;
        cacheEntry.softTtl = softExpire;
        cacheEntry.ttl = ttl;
        String headerValue;
        headerValue = response.headers.get("Date");
        if (headerValue != null) {
            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }
        headerValue = response.headers.get("Last-Modified");
        if (headerValue != null) {
            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }
        cacheEntry.responseHeaders = response.headers;
        return Response.success(response, cacheEntry);
    }

    @Override
    protected void deliverResponse(NetworkResponse response) {
        if (mListener != null) {
            mListener.onResponse(response);
            Log.e("cacheResponse", "cachedResponse" + response);
        }

    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        //return super.parseNetworkError(volleyError);
        if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
            volleyError = new VolleyError(new String(volleyError.networkResponse.data));

            if (volleyError instanceof TimeoutError || volleyError instanceof NoConnectionError) {
                //This indicates that the reuest has either time out or there is no connection
            } else if (volleyError instanceof AuthFailureError) {
                //Error indicating that there was an Authentication Failure while performing the request
            } else if (volleyError instanceof ServerError) {
                //Indicates that the server responded with a error response
            } else if (volleyError instanceof NetworkError) {
                //Indicates that there was network error while performing the request
            } else if (volleyError instanceof ParseError) {
                // Indicates that the server response could not be parsed
            }
        }
        return volleyError;
    }

    @Override
    public void deliverError(VolleyError error) {
        mErrorListener.onErrorResponse(error);
    }


    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
            return null;
        }
    }

    @Override
    public String getBodyContentType() {
        return mContentType;
    }
}


//    public static Cache.Entry parseIgnoreCacheHeaders(NetworkResponse response) {
//        long now = System.currentTimeMillis();
//        Map<String, String> headers = response.headers;
//        long serverDate = 0;
//        String serverEtag = null;
//        String headerValue;
//
//        headerValue = headers.get("Date");
//        if (headerValue != null) {
//            serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
//        }
//
//        serverEtag = headers.get("ETag");
//
//        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
//        final long softExpire = now + cacheHitButRefreshed;
//        final long ttl = now + cacheExpired;
//
//        Cache.Entry entry = new Cache.Entry();
//        entry.data = response.data;
//        entry.etag = serverEtag;
//        entry.softTtl = softExpire; //soft time to live in milliseconds
//        entry.ttl = ttl; //time to live in milliseconds
//        entry.serverDate = serverDate;
//        entry.responseHeaders = headers;
//
//        return entry;
//    }
//
//    public String getErrorMessageForCode(int statusCode) {
//        //TODO: Generate appropriate error message here
//
//        String errorMsg = mContext.getResources().getString(R.string.deafult_error);
//        switch (statusCode) {
//            case 400:
//                errorMsg = mContext.getResources().getString(R.string.bad_request);
//                break;
//            case 401:
//                errorMsg = mContext.getResources().getString(R.string.Forbidden);
//                break;
//            case 403:
//                errorMsg = mContext.getResources().getString(R.string.Forbidden);
//                break;
//            case 405:
//                errorMsg = mContext.getResources().getString(R.string.Server_Error);
//                break;
//            case 500:
//                errorMsg = mContext.getResources().getString(R.string.Internal_Server_Error);
//                break;
//            case 502:
//                errorMsg = mContext.getResources().getString(R.string.Bad_Gateway);
//                break;
//            case 503:
//                errorMsg = mContext.getResources().getString(R.string.deafult_error);
//                break;
//            case 504:
//                errorMsg = mContext.getResources().getString(R.string.deafult_error);
//                break;
//        }
//        return errorMsg;
//    }
