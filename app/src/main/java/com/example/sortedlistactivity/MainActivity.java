package com.example.sortedlistactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.provider.SyncStateContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.sortedlistactivity.GsonModels.Data;
import com.example.sortedlistactivity.GsonModels.DataResponse;
import com.example.sortedlistactivity.GsonModels.Objects;
import com.example.sortedlistactivity.helper.CacheStringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


public class MainActivity extends AppCompatActivity {
    private static final int MY_SOCKET_TIMEOUT_MS = 60000;
    public RequestQueue requestQueue;

    public List<DatesModel> datesModelList = new ArrayList<>();
    List<SortedModel> sortedModelList = new ArrayList<>();
    List<DatesModel> demoSortedModelList = new ArrayList<>();

    String url = "https://api.myjson.com/bins/uf2y8";
    String url2 = "https://api.myjson.com/bins/16mguo";
    String url3 = "https://api.myjson.com/bins/oxw6o";
    String url4 = "https://api.myjson.com/bins/13w3yg";
    String url5 = "https://api.myjson.com/bins/17kzwo";
    String TableDataURL = "http://192.168.1.18/HIMS_rRAMS/MobileServices/assets/InspectionGroup/MBS/Table";
    private SortedAdapter sortedAdapter;
    private RecyclerView recyclerView;
    private int num;
    private DatesModel datesModel;
    private List<SortedModel> modelList;
    private List<Data> dataList;
    private List<Objects> objectsList;

    // Function to remove duplicates from an ArrayList
    public static List<SortedModel> removeDuplicates(List<SortedModel> list) {
        // Create a new LinkedHashSet
        Set<SortedModel> set = new HashSet<>();
        // Add the elements to set
        set.addAll(list);

        // Clear the list
        list.clear();

        // add the elements of set
        // with no duplicates to the list
        list.addAll(set);
        for (int i = 0; i < list.size(); i++)
            Log.e("duplicates", "data " + list.get(i).getDate());
        // return the list
        return list;
    }

    public static List<SortedModel> getDuplicates(List<SortedModel> sortedModelList) {
        Set<String> dupes = new HashSet<>();
        for (SortedModel sortedModel : sortedModelList) {
            if (!dupes.add(sortedModel.getDate())) {
                DatesModel datesModel = new DatesModel();
                datesModel.setDates(sortedModel.getDate());
                datesModel.setAmount(sortedModel.getAmount());
                datesModel.setNames(sortedModel.getName());

                List<DatesModel> datesModelList = new ArrayList<>();
                datesModelList.add(datesModel);
                Log.e("duplicates", "Duplicate element in array is Name : " + sortedModel.getName());
                Log.e("duplicates", "Duplicate element in array is Amount : " + sortedModel.getAmount());
                Log.e("duplicates", "Duplicate element in array is Date : " + sortedModel.getDate());
            } else {
                List<SortedModel> modelList = new ArrayList<>();
                SortedModel sortedModel1 = new SortedModel();
                sortedModel.setDate(sortedModel1.getDate());
                sortedModel.setAmount(sortedModel1.getAmount());
                sortedModel1.setName(sortedModel1.getName());
                modelList.add(sortedModel);
                Log.e("duplicates", "dups : " + sortedModel.getDate());

            }
        }
        return sortedModelList;
    }

    public void printDatesInMonth(int year, int month) {
        SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(year, month - 1, 1);
        datesModel = new DatesModel();
        int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        datesModelList = new ArrayList<>();
        for (int i = 0; i < daysInMonth; i++) {
            System.out.println(fmt.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
            Log.e("dateFormat", "formatted " + fmt.format(cal.getTime()));
            DatesModel datesModel = new DatesModel();
            datesModel.setDates(fmt.format(cal.getTime()));
            datesModelList.add(datesModel);
        }

//        for (int k = 0; k < datesModelList.size(); k++) {
//            String myDataModels = datesModelList.get(k).getDates();
//            Log.e("dateFormat", "myDataModels " + myDataModels);
//
//        }
//        Log.e("dateFormat", "datesModelList " + datesModelList.size());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestQueue = Volley.newRequestQueue(MainActivity.this);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);

        //recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        sortedAdapter = new SortedAdapter(MainActivity.this, sortedModelList);
        recyclerView.setAdapter(sortedAdapter);


        //   myGsonData();

        CacheStringRequest cacheStringRequest = new CacheStringRequest(MainActivity.this, Request.Method.GET, TableDataURL,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        dataList = new ArrayList<>();
                        objectsList = new ArrayList<>();

                        try {
                            final String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

                            DataResponse dataResponse = new Gson().fromJson(jsonString, DataResponse.class);
                            Log.e("dataResponse", "res " + dataResponse);

                            dataList = dataResponse.getData();
                            Log.e("dataResponse", "dataList " + dataList.toString());

                            objectsList = new Gson().fromJson(dataResponse.getData().toString(), new TypeToken<List<Objects>>() {
                            }.getType());
                            Log.e("dataResponse", "objectsList " + objectsList.toString());

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("dataResponse", "VolleyError " + error);
            }
        });
        requestQueue.add(cacheStringRequest);
        cacheStringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url5, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        sortedModelList = new Gson().fromJson(response.toString(), new TypeToken<List<SortedModel>>() {
                        }.getType());

                        //  getDuplicates(sortedModelList);

                        Set<String> dupes = new HashSet<>();

                        modelList = new ArrayList<>();
                        datesModelList = new ArrayList<>();

                        //this is for loop
                        for (SortedModel sortedModel : sortedModelList) {
                            //set finding duplicates and will be removed
                            if (!dupes.add(sortedModel.getDate())) {
                                DatesModel datesModel = new DatesModel();
                                datesModel.setDates(sortedModel.getDate());
                                datesModel.setAmount(sortedModel.getAmount());
                                datesModel.setNames(sortedModel.getName());
                                datesModelList.add(datesModel);
                                Log.e("duplicates", "Duplicate element in array is Name : " + sortedModel.getName());
                                Log.e("duplicates", "Duplicate element in array is Amount : " + sortedModel.getAmount());
                                Log.e("duplicates", "Duplicate element in array is Date : " + sortedModel.getDate());

                            } else {
                                SortedModel dupSortModel = new SortedModel();
                                dupSortModel.setDate(sortedModel.getDate());
                                dupSortModel.setAmount(sortedModel.getAmount());
                                dupSortModel.setName(sortedModel.getName());
                                modelList.add(dupSortModel);
                                Log.e("duplicates", "dups : " + dupSortModel.getDate());
                            }
                        }


                        sortedAdapter = new SortedAdapter(MainActivity.this, sortedModelList);
                        Log.e("myNames", "datesModelList2 " + datesModelList);
                        recyclerView.setAdapter(sortedAdapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonArrayRequest);
    }

    private void myGsonData() {
        requestQueue = Volley.newRequestQueue(MainActivity.this);
        dataList = new ArrayList<>();
        objectsList = new ArrayList<>();

        String url = "http://192.168.1.18/HIMS_rRAMS/MobileServices/assets/InspectionGroup/MBS/Table";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        DataResponse dataResponse = new Gson().fromJson(response.toString(), DataResponse.class);
                        Log.e("dataResponse", "res " + dataResponse);

                        dataList = dataResponse.getData();
                        Log.e("dataResponse", "dataList " + dataList.toString());

                        objectsList = new Gson().fromJson(dataResponse.getData().toString(), new TypeToken<List<Objects>>() {
                        }.getType());
                        Log.e("dataResponse", "objectsList " + objectsList.toString());

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public List<SortedModel> getSortedByNames() {
        final List<SortedModel> employeeList = getNamesOrDatesModelList();
        Log.e("modelList", "myList " + employeeList);
        if (employeeList != null && employeeList.size() > 0) {
            Collections.sort(employeeList, new Comparator<SortedModel>() {
                @Override
                public int compare(SortedModel a1, SortedModel a2) {
                    return a1.getName().compareTo(a2.getName());
                }
            });
        }
        return employeeList;
    }

    public List<SortedModel> getSortedByDates() {
        final List<SortedModel> employeeList = getSortedModelList();
        Log.e("modelList", "myList " + employeeList);
        if (employeeList != null && employeeList.size() > 0) {
            Collections.sort(employeeList, new Comparator<SortedModel>() {
                @Override
                public int compare(SortedModel a1, SortedModel a2) {
                    Set<SortedModel> dupes = new HashSet<SortedModel>();
                    for (SortedModel i : getSortedModelList()) {
                        if (!dupes.add(i)) {
                            Log.e("duplicates", "Duplicate element in array is : " + i);
                        }
                    }
                    return a1.getDate().compareTo(a2.getDate());
                }
            });
        }
        return employeeList;
    }

    public List<SortedModel> getSorteddByAmountAsc() {
        final List<SortedModel> employeeList = getNamesOrDatesModelList();
        Log.e("modelList", "myList " + employeeList);
        if (employeeList != null && employeeList.size() > 0) {
            Collections.sort(employeeList, new Comparator<SortedModel>() {
                @Override
                public int compare(SortedModel a1, SortedModel a2) {
                    //                return a1.getAmount().compareTo(a2.getAmount());
                    Integer integer1 = Integer.valueOf(a1.getAmount());
                    Integer integer2 = Integer.valueOf(a2.getAmount());
                    return integer1.compareTo(integer2);
                }
            });
        }
        return employeeList;
    }

    public List<SortedModel> getSorteddByAmountDesc() {
        final List<SortedModel> employeeList = getNamesOrDatesModelList();
        Log.e("modelList", "myList " + employeeList);
        if (employeeList != null && employeeList.size() > 0) {
            Collections.sort(employeeList, new Comparator<SortedModel>() {
                @Override
                public int compare(SortedModel a1, SortedModel a2) {
                    //                return a1.getAmount().compareTo(a2.getAmount());
                    if (a1.getAmount() == a2.getAmount()) {
                        //                    return 1;
                        num = 1;
                    } else {
                        num = -1;
                        //                    return -1;
                    }
                    return num;
                }
            });
        }
        return employeeList;
    }

    private SortedModel getDateWiseData(String date) {
        SortedModel sortedModel = new SortedModel();
        List<SortedModel> sortedModelList = getSortedModelList();
        for (int i = 0; i < sortedModelList.size(); i++) {
            String stringDate = sortedModelList.get(i).getDate().trim();
            if (stringDate.trim().equalsIgnoreCase(date)) {
                sortedModel = sortedModelList.get(i);
                break;
            }
        }
        return sortedModel;
    }


    public List<SortedModel> getSortedModelList() {
        return modelList;
    }

    public List<SortedModel> getNamesOrDatesModelList() {
        return sortedModelList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.OrderByNames:
                // Action goes here
                sortedAdapter = new SortedAdapter(MainActivity.this, getSortedByNames());
                recyclerView.setAdapter(sortedAdapter);
                sortedAdapter.notifyDataSetChanged();
                return true;
            case R.id.OrderByDates:
                sortedAdapter = new SortedAdapter(MainActivity.this, getSortedByDates(), datesModelList);
                recyclerView.setAdapter(sortedAdapter);
                sortedAdapter.notifyDataSetChanged();
                return true;
            case R.id.OrderByAmountAsc:
                sortedAdapter = new SortedAdapter(MainActivity.this, getSorteddByAmountAsc());
                recyclerView.setAdapter(sortedAdapter);
                sortedAdapter.notifyDataSetChanged();
                return true;
            case R.id.OrderByAmountDsc:
                sortedAdapter = new SortedAdapter(MainActivity.this, getSorteddByAmountDesc());
                recyclerView.setAdapter(sortedAdapter);
                sortedAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
