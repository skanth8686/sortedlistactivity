package com.example.sortedlistactivity;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class SortedAdapter extends RecyclerView.Adapter<SortedAdapter.MyViewHolder> {
    public List<SortedModel> sortedModelList = new ArrayList<>();
    public List<SortedModel> demoSortedModelList = new ArrayList<>();
    public Context context;
    public List<DatesModel> datesModelList = new ArrayList<>();

    public SortedAdapter(Context context, List<SortedModel> sortedModelList) {
        this.sortedModelList = sortedModelList;
        this.context = context;
    }

    public SortedAdapter(Context context, List<SortedModel> sortedModelList, List<DatesModel> datesModelList) {
        this.sortedModelList = sortedModelList;
        this.context = context;
        this.datesModelList = datesModelList;
    }

    public SortedAdapter(List<SortedModel> sortedModelList, List<SortedModel> demoSortedModelList, Context context) {
        this.sortedModelList = sortedModelList;
        this.demoSortedModelList = demoSortedModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.sortelist, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        SortedModel sortedModel = sortedModelList.get(position);

        holder.childCardView.setVisibility(View.GONE);

        for (DatesModel datesModel : datesModelList) {
            if (datesModel.getDates().equals(sortedModelList.get(position).getDate())) {
                Log.e("myNames", "names " + datesModel.getNames());
                Log.e("myNames", "dates " + datesModel.getDates());
                Log.e("myNames", "amount " + datesModel.getAmount());

                Log.e("myNames", "names1 " + sortedModelList.get(position).getName());
                Log.e("myNames", "dates1 " + sortedModelList.get(position).getDate());
                Log.e("myNames", "amount1 " + sortedModelList.get(position).getAmount());
                holder.childCardView.setVisibility(View.VISIBLE);

                holder.getTextNames1.setText(datesModel.getNames());
                holder.getTextAmounts1.setText(datesModel.getAmount());
            } else {
                holder.childCardView.setVisibility(View.GONE);
            }
        }

        holder.textNames.setText(sortedModel.getName());
        holder.textDates.setText(sortedModel.getDate());
        holder.textAmounts.setText(sortedModel.getAmount());

    }

    @Override
    public int getItemCount() {
        return sortedModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textNames, textDates, textAmounts;
        public TextView getTextNames1, getTextAmounts1;
        public LinearLayout dupslinearLayout;
        public CardView childCardView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textNames = itemView.findViewById(R.id.textNames);
            textDates = itemView.findViewById(R.id.textDates);
            textAmounts = itemView.findViewById(R.id.textAmounts);

            //duplicates
            childCardView = itemView.findViewById(R.id.childCardView);
            dupslinearLayout = itemView.findViewById(R.id.dupsLayout);
            getTextNames1 = itemView.findViewById(R.id.textNames1);
            getTextAmounts1 = itemView.findViewById(R.id.textAmounts1);

        }
    }


}
