package com.example.sortedlistactivity.GsonModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Awesome Pojo Generator
 */
public class Data {
    @SerializedName("ReportObjects")
    @Expose
    private Object ReportObjects;
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("Objects")
    @Expose
    private List<Objects> Objects;
    @SerializedName("SectionObjects")
    @Expose
    private Object SectionObjects;
    @SerializedName("BPObjects")
    @Expose
    private Object BPObjects;
    @SerializedName("LDescription")
    @Expose
    private Object LDescription;
    @SerializedName("Caption")
    @Expose
    private String Caption;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("FormulaObjects")
    @Expose
    private Object FormulaObjects;
    @SerializedName("ObjectType")
    @Expose
    private String ObjectType;
    @SerializedName("Items")
    @Expose
    private Object Items;
    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("LCaption")
    @Expose
    private String LCaption;
    @SerializedName("Module")
    @Expose
    private String Module;
    @SerializedName("DCObjects")
    @Expose
    private Object DCObjects;

    public Object getReportObjects() {
        return ReportObjects;
    }

    public void setReportObjects(Object ReportObjects) {
        this.ReportObjects = ReportObjects;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public List<Objects> getObjects() {
        return Objects;
    }

    public void setObjects(List<Objects> Objects) {
        this.Objects = Objects;
    }

    public Object getSectionObjects() {
        return SectionObjects;
    }

    public void setSectionObjects(Object SectionObjects) {
        this.SectionObjects = SectionObjects;
    }

    public Object getBPObjects() {
        return BPObjects;
    }

    public void setBPObjects(Object BPObjects) {
        this.BPObjects = BPObjects;
    }

    public Object getLDescription() {
        return LDescription;
    }

    public void setLDescription(Object LDescription) {
        this.LDescription = LDescription;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String Caption) {
        this.Caption = Caption;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Object getFormulaObjects() {
        return FormulaObjects;
    }

    public void setFormulaObjects(Object FormulaObjects) {
        this.FormulaObjects = FormulaObjects;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setObjectType(String ObjectType) {
        this.ObjectType = ObjectType;
    }

    public Object getItems() {
        return Items;
    }

    public void setItems(Object Items) {
        this.Items = Items;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getLCaption() {
        return LCaption;
    }

    public void setLCaption(String LCaption) {
        this.LCaption = LCaption;
    }

    public String getModule() {
        return Module;
    }

    public void setModule(String Module) {
        this.Module = Module;
    }

    public Object getDCObjects() {
        return DCObjects;
    }

    public void setDCObjects(Object DCObjects) {
        this.DCObjects = DCObjects;
    }
}