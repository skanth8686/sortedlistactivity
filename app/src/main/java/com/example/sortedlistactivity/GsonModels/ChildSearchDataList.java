package com.example.sortedlistactivity.GsonModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 */
public class ChildSearchDataList {
    @SerializedName("Val")
    @Expose
    private String Val;
    @SerializedName("Key")
    @Expose
    private Integer Key;

    public String getVal() {
        return Val;
    }

    public void setVal(String Val) {
        this.Val = Val;
    }

    public Integer getKey() {
        return Key;
    }

    public void setKey(Integer Key) {
        this.Key = Key;
    }
}