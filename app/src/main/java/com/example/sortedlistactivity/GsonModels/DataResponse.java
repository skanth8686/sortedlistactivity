package com.example.sortedlistactivity.GsonModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Awesome Pojo Generator
 */
public class DataResponse {
    @SerializedName("Status")
    @Expose
    private Boolean Status;
    @SerializedName("Message")
    @Expose
    private Object Message;
    @SerializedName("Data")
    @Expose
    private List<Data> Data;
    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("Caption")
    @Expose
    private String Caption;

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean Status) {
        this.Status = Status;
    }

    public Object getMessage() {
        return Message;
    }

    public void setMessage(Object Message) {
        this.Message = Message;
    }

    public List<Data> getData() {
        return Data;
    }

    public void setData(List<Data> Data) {
        this.Data = Data;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String Caption) {
        this.Caption = Caption;
    }
}