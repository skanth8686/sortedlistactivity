package com.example.sortedlistactivity.GsonModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Awesome Pojo Generator
 */
public class SearchDataLst {
    @SerializedName("ColumnName")
    @Expose
    private String ColumnName;
    @SerializedName("MobiSearchDataList")
    @Expose
    private List<MobiSearchDataList> MobiSearchDataList;
    @SerializedName("ChildSearchDataList")
    @Expose
    private List<ChildSearchDataList> ChildSearchDataList;

    public String getColumnName() {
        return ColumnName;
    }

    public void setColumnName(String ColumnName) {
        this.ColumnName = ColumnName;
    }

    public List<MobiSearchDataList> getMobiSearchDataList() {
        return MobiSearchDataList;
    }

    public void setMobiSearchDataList(List<MobiSearchDataList> MobiSearchDataList) {
        this.MobiSearchDataList = MobiSearchDataList;
    }

    public List<ChildSearchDataList> getChildSearchDataList() {
        return ChildSearchDataList;
    }

    public void setChildSearchDataList(List<ChildSearchDataList> ChildSearchDataList) {
        this.ChildSearchDataList = ChildSearchDataList;
    }
}