package com.example.sortedlistactivity.GsonModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Awesome Pojo Generator
 */
public class ColumsList {
    @SerializedName("DataTable")
    @Expose
    private String DataTable;
    @SerializedName("Minimum")
    @Expose
    private String Minimum;
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("ColumnType")
    @Expose
    private Boolean ColumnType;
    @SerializedName("DefaultVal")
    @Expose
    private String DefaultVal;
    @SerializedName("SearchDataLst")
    @Expose
    private List<SearchDataLst> SearchDataLst;
    @SerializedName("PrimaryKey")
    @Expose
    private Boolean PrimaryKey;
    @SerializedName("AllowNulls")
    @Expose
    private Boolean AllowNulls;
    @SerializedName("LDescription")
    @Expose
    private String LDescription;
    @SerializedName("DataStoreType")
    @Expose
    private String DataStoreType;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Comma")
    @Expose
    private Boolean Comma;
    @SerializedName("Maximum")
    @Expose
    private String Maximum;
    @SerializedName("Currency")
    @Expose
    private Boolean Currency;
    @SerializedName("Visible")
    @Expose
    private Boolean Visible;
    @SerializedName("Editable")
    @Expose
    private Boolean Editable;
    @SerializedName("IsFKRefMastercol")
    @Expose
    private Boolean IsFKRefMastercol;
    @SerializedName("FKReference")
    @Expose
    private Object FKReference;
    @SerializedName("SplitClass")
    @Expose
    private String SplitClass;
    @SerializedName("ShowInMap")
    @Expose
    private Boolean ShowInMap;
    @SerializedName("FKRefTableName")
    @Expose
    private Object FKRefTableName;
    @SerializedName("ShowInMobile")
    @Expose
    private Boolean ShowInMobile;
    @SerializedName("SearchProperty")
    @Expose
    private Boolean SearchProperty;
    @SerializedName("DisplayFormat")
    @Expose
    private Object DisplayFormat;
    @SerializedName("Sequence")
    @Expose
    private Integer Sequence;
    @SerializedName("Decimals")
    @Expose
    private Object Decimals;
    @SerializedName("DefaultClass")
    @Expose
    private Object DefaultClass;
    @SerializedName("Caption")
    @Expose
    private String Caption;
    @SerializedName("ValidationRule")
    @Expose
    private Boolean ValidationRule;
    @SerializedName("MasterColumn")
    @Expose
    private Boolean MasterColumn;
    @SerializedName("IsChart")
    @Expose
    private Boolean IsChart;
    @SerializedName("AssetType")
    @Expose
    private Object AssetType;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("ValidationRuleName")
    @Expose
    private Object ValidationRuleName;
    @SerializedName("ControlType")
    @Expose
    private String ControlType;
    @SerializedName("ValidationExpr")
    @Expose
    private Object ValidationExpr;
    @SerializedName("MergeClass")
    @Expose
    private String MergeClass;
    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("LCaption")
    @Expose
    private String LCaption;
    @SerializedName("FKRefDisplay")
    @Expose
    private Object FKRefDisplay;
    @SerializedName("ValidationText")
    @Expose
    private Object ValidationText;

    public String getDataTable() {
        return DataTable;
    }

    public void setDataTable(String DataTable) {
        this.DataTable = DataTable;
    }

    public String getMinimum() {
        return Minimum;
    }

    public void setMinimum(String Minimum) {
        this.Minimum = Minimum;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Boolean getColumnType() {
        return ColumnType;
    }

    public void setColumnType(Boolean ColumnType) {
        this.ColumnType = ColumnType;
    }

    public String getDefaultVal() {
        return DefaultVal;
    }

    public void setDefaultVal(String DefaultVal) {
        this.DefaultVal = DefaultVal;
    }

    public List<SearchDataLst> getSearchDataLst() {
        return SearchDataLst;

    }

    public void setSearchDataLst(List<SearchDataLst> SearchDataLst) {
        this.SearchDataLst = SearchDataLst;
    }

    public Boolean getPrimaryKey() {
        return PrimaryKey;
    }

    public void setPrimaryKey(Boolean PrimaryKey) {
        this.PrimaryKey = PrimaryKey;
    }

    public Boolean getAllowNulls() {
        return AllowNulls;
    }

    public void setAllowNulls(Boolean AllowNulls) {
        this.AllowNulls = AllowNulls;
    }

    public String getLDescription() {
        return LDescription;
    }

    public void setLDescription(String LDescription) {
        this.LDescription = LDescription;
    }

    public String getDataStoreType() {
        return DataStoreType;
    }

    public void setDataStoreType(String DataStoreType) {
        this.DataStoreType = DataStoreType;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Boolean getComma() {
        return Comma;
    }

    public void setComma(Boolean Comma) {
        this.Comma = Comma;
    }

    public String getMaximum() {
        return Maximum;
    }

    public void setMaximum(String Maximum) {
        this.Maximum = Maximum;
    }

    public Boolean getCurrency() {
        return Currency;
    }

    public void setCurrency(Boolean Currency) {
        this.Currency = Currency;
    }

    public Boolean getVisible() {
        return Visible;
    }

    public void setVisible(Boolean Visible) {
        this.Visible = Visible;
    }

    public Boolean getEditable() {
        return Editable;
    }

    public void setEditable(Boolean Editable) {
        this.Editable = Editable;
    }

    public Boolean getIsFKRefMastercol() {
        return IsFKRefMastercol;
    }

    public void setIsFKRefMastercol(Boolean IsFKRefMastercol) {
        this.IsFKRefMastercol = IsFKRefMastercol;
    }

    public Object getFKReference() {
        return FKReference;
    }

    public void setFKReference(Object FKReference) {
        this.FKReference = FKReference;
    }

    public String getSplitClass() {
        return SplitClass;
    }

    public void setSplitClass(String SplitClass) {
        this.SplitClass = SplitClass;
    }

    public Boolean getShowInMap() {
        return ShowInMap;
    }

    public void setShowInMap(Boolean ShowInMap) {
        this.ShowInMap = ShowInMap;
    }

    public Object getFKRefTableName() {
        return FKRefTableName;
    }

    public void setFKRefTableName(Object FKRefTableName) {
        this.FKRefTableName = FKRefTableName;
    }

    public Boolean getShowInMobile() {
        return ShowInMobile;
    }

    public void setShowInMobile(Boolean ShowInMobile) {
        this.ShowInMobile = ShowInMobile;
    }

    public Boolean getSearchProperty() {
        return SearchProperty;
    }

    public void setSearchProperty(Boolean SearchProperty) {
        this.SearchProperty = SearchProperty;
    }

    public Object getDisplayFormat() {
        return DisplayFormat;
    }

    public void setDisplayFormat(Object DisplayFormat) {
        this.DisplayFormat = DisplayFormat;
    }

    public Integer getSequence() {
        return Sequence;
    }

    public void setSequence(Integer Sequence) {
        this.Sequence = Sequence;
    }

    public Object getDecimals() {
        return Decimals;
    }

    public void setDecimals(Object Decimals) {
        this.Decimals = Decimals;
    }

    public Object getDefaultClass() {
        return DefaultClass;
    }

    public void setDefaultClass(Object DefaultClass) {
        this.DefaultClass = DefaultClass;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String Caption) {
        this.Caption = Caption;
    }

    public Boolean getValidationRule() {
        return ValidationRule;
    }

    public void setValidationRule(Boolean ValidationRule) {
        this.ValidationRule = ValidationRule;
    }

    public Boolean getMasterColumn() {
        return MasterColumn;
    }

    public void setMasterColumn(Boolean MasterColumn) {
        this.MasterColumn = MasterColumn;
    }

    public Boolean getIsChart() {
        return IsChart;
    }

    public void setIsChart(Boolean IsChart) {
        this.IsChart = IsChart;
    }

    public Object getAssetType() {
        return AssetType;
    }

    public void setAssetType(Object AssetType) {
        this.AssetType = AssetType;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Object getValidationRuleName() {
        return ValidationRuleName;
    }

    public void setValidationRuleName(Object ValidationRuleName) {
        this.ValidationRuleName = ValidationRuleName;
    }

    public String getControlType() {
        return ControlType;
    }

    public void setControlType(String ControlType) {
        this.ControlType = ControlType;
    }

    public Object getValidationExpr() {
        return ValidationExpr;
    }

    public void setValidationExpr(Object ValidationExpr) {
        this.ValidationExpr = ValidationExpr;
    }

    public String getMergeClass() {
        return MergeClass;
    }

    public void setMergeClass(String MergeClass) {
        this.MergeClass = MergeClass;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getLCaption() {
        return LCaption;
    }

    public void setLCaption(String LCaption) {
        this.LCaption = LCaption;
    }

    public Object getFKRefDisplay() {
        return FKRefDisplay;
    }

    public void setFKRefDisplay(Object FKRefDisplay) {
        this.FKRefDisplay = FKRefDisplay;
    }

    public Object getValidationText() {
        return ValidationText;
    }

    public void setValidationText(Object ValidationText) {
        this.ValidationText = ValidationText;
    }
}