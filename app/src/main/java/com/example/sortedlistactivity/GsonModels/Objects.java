package com.example.sortedlistactivity.GsonModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Awesome Pojo Generator
 */
public class Objects {
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("orderseq")
    @Expose
    private Integer orderseq;
    @SerializedName("lstTabledata")
    @Expose
    private Object lstTabledata;
    @SerializedName("LDescription")
    @Expose
    private String LDescription;
    @SerializedName("Image")
    @Expose
    private Boolean Image;
    @SerializedName("Document")
    @Expose
    private Boolean Document;
    @SerializedName("ParentName")
    @Expose
    private Object ParentName;
    @SerializedName("selectedTableNamesUnlock")
    @Expose
    private Boolean selectedTableNamesUnlock;
    @SerializedName("Global")
    @Expose
    private Boolean Global;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("LCode")
    @Expose
    private String LCode;
    @SerializedName("IsPublic")
    @Expose
    private Boolean IsPublic;
    @SerializedName("RoadCode")
    @Expose
    private Object RoadCode;
    @SerializedName("ShowInMap")
    @Expose
    private Boolean ShowInMap;
    @SerializedName("IsMobile")
    @Expose
    private Boolean IsMobile;
    @SerializedName("Status")
    @Expose
    private Boolean Status;
    @SerializedName("ReadOnly")
    @Expose
    private Boolean ReadOnly;
    @SerializedName("SurveyID")
    @Expose
    private Object SurveyID;
    @SerializedName("IsParent")
    @Expose
    private Boolean IsParent;
    @SerializedName("LinkCode")
    @Expose
    private Object LinkCode;
    @SerializedName("Sequence")
    @Expose
    private Integer Sequence;
    @SerializedName("ColumsList")
    @Expose
    private List<ColumsList> ColumsList;
    @SerializedName("Caption")
    @Expose
    private String Caption;
    @SerializedName("selectedTableNames")
    @Expose
    private Boolean selectedTableNames;
    @SerializedName("Type")
    @Expose
    private String Type;
    @SerializedName("MapImage")
    @Expose
    private Object MapImage;
    @SerializedName("Video")
    @Expose
    private Boolean Video;
    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("DataReference")
    @Expose
    private String DataReference;
    @SerializedName("lstlocktables")
    @Expose
    private Object lstlocktables;
    @SerializedName("checklock")
    @Expose
    private Object checklock;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Integer getOrderseq() {
        return orderseq;
    }

    public void setOrderseq(Integer orderseq) {
        this.orderseq = orderseq;
    }

    public Object getLstTabledata() {
        return lstTabledata;
    }

    public void setLstTabledata(Object lstTabledata) {
        this.lstTabledata = lstTabledata;
    }

    public String getLDescription() {
        return LDescription;
    }

    public void setLDescription(String LDescription) {
        this.LDescription = LDescription;
    }

    public Boolean getImage() {
        return Image;
    }

    public void setImage(Boolean Image) {
        this.Image = Image;
    }

    public Boolean getDocument() {
        return Document;
    }

    public void setDocument(Boolean Document) {
        this.Document = Document;
    }

    public Object getParentName() {
        return ParentName;
    }

    public void setParentName(Object ParentName) {
        this.ParentName = ParentName;
    }

    public Boolean getSelectedTableNamesUnlock() {
        return selectedTableNamesUnlock;
    }

    public void setSelectedTableNamesUnlock(Boolean selectedTableNamesUnlock) {
        this.selectedTableNamesUnlock = selectedTableNamesUnlock;
    }

    public Boolean getGlobal() {
        return Global;
    }

    public void setGlobal(Boolean Global) {
        this.Global = Global;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getLCode() {
        return LCode;
    }

    public void setLCode(String LCode) {
        this.LCode = LCode;
    }

    public Boolean getIsPublic() {
        return IsPublic;
    }

    public void setIsPublic(Boolean IsPublic) {
        this.IsPublic = IsPublic;
    }

    public Object getRoadCode() {
        return RoadCode;
    }

    public void setRoadCode(Object RoadCode) {
        this.RoadCode = RoadCode;
    }

    public Boolean getShowInMap() {
        return ShowInMap;
    }

    public void setShowInMap(Boolean ShowInMap) {
        this.ShowInMap = ShowInMap;
    }

    public Boolean getIsMobile() {
        return IsMobile;
    }

    public void setIsMobile(Boolean IsMobile) {
        this.IsMobile = IsMobile;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean Status) {
        this.Status = Status;
    }

    public Boolean getReadOnly() {
        return ReadOnly;
    }

    public void setReadOnly(Boolean ReadOnly) {
        this.ReadOnly = ReadOnly;
    }

    public Object getSurveyID() {
        return SurveyID;
    }

    public void setSurveyID(Object SurveyID) {
        this.SurveyID = SurveyID;
    }

    public Boolean getIsParent() {
        return IsParent;
    }

    public void setIsParent(Boolean IsParent) {
        this.IsParent = IsParent;
    }

    public Object getLinkCode() {
        return LinkCode;
    }

    public void setLinkCode(Object LinkCode) {
        this.LinkCode = LinkCode;
    }

    public Integer getSequence() {
        return Sequence;
    }

    public void setSequence(Integer Sequence) {
        this.Sequence = Sequence;
    }

    public List<ColumsList> getColumsList() {
        return ColumsList;
    }

    public void setColumsList(List<ColumsList> ColumsList) {
        this.ColumsList = ColumsList;
    }

    public String getCaption() {
        return Caption;
    }

    public void setCaption(String Caption) {
        this.Caption = Caption;
    }

    public Boolean getSelectedTableNames() {
        return selectedTableNames;
    }

    public void setSelectedTableNames(Boolean selectedTableNames) {
        this.selectedTableNames = selectedTableNames;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public Object getMapImage() {
        return MapImage;
    }

    public void setMapImage(Object MapImage) {
        this.MapImage = MapImage;
    }

    public Boolean getVideo() {
        return Video;
    }

    public void setVideo(Boolean Video) {
        this.Video = Video;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getDataReference() {
        return DataReference;
    }

    public void setDataReference(String DataReference) {
        this.DataReference = DataReference;
    }

    public Object getLstlocktables() {
        return lstlocktables;
    }

    public void setLstlocktables(Object lstlocktables) {
        this.lstlocktables = lstlocktables;
    }

    public Object getChecklock() {
        return checklock;
    }

    public void setChecklock(Object checklock) {
        this.checklock = checklock;
    }
}